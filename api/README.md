# url-shortener-api
Express API providing create, read & delete actions. Uses to MongoDB.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev:watch
```

### Build & run for production
```
npm run build && npm run start
```