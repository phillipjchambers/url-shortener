
import { Request, Response } from "express";
import Url from "../models/url";

class UrlController {
    allUrls(req: Request, res: Response) {
        Url.find((err: any, urls: any) => {
            if (err) {
                res.send(err);
            } else {
                res.send(urls);
            }
        });
    }

    addUrl(req: Request, res: Response) {
        const url = new Url(req.body);
        url.save((err: any) => {
            if (err) {
                res.send(err);
            } else {
                res.send(url);
            }
        });
    }

    deletUrl(req: Request, res: Response) {
        Url.deleteOne({ _id: req.params.id }, (err: any) => {
            if (err) {
                res.send(err);
            } else {
                res.send("Url deleted from database");
            }
        });
    }
}

export default UrlController;

// export const allUrls = (req: Request, res: Response) => {
//     const result = Url.find((err: any, urls: any) => {
//         if (err) {
//             res.send(err);
//         } else {
//             res.send(urls);
//         }
//     });
// };

// export const addUrl = (req: Request, res: Response) => {
//     const url = new Url(req.body);
//     url.save((err: any) => {
//         if (err) {
//             res.send(err);
//         } else {
//             res.send(url);
//         }
//     });
// };

// export const deleteUrl = (req: Request, res: Response) => {
//     const result = Url.deleteOne({ _id: req.params.id }, (err: any) => {
//         if (err) {
//             res.send(err);
//         } else {
//             res.send("Url deleted from database");
//         }
//     });
// };