import cors from "cors";
import connect from "./connect";
import UrlController from "./controllers/url.controller";
import { dbConfig } from "./config/db.config";
import * as bodyParser from "body-parser";
import express from "express";

class App {
    private defaultPort = 8080;
    public app: express.Application;
    private url: UrlController;

    constructor() {
        this.app = express();
        this.url = new UrlController();

        this.config();
        this.connect();
        this.listen(process.env.PORT || this.defaultPort);
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));

        const corsOptions = {
            origin: "http://localhost:3000"
        };

        this.app.use(cors(corsOptions));

        this.app.get("/urls", this.url.allUrls);
        this.app.post("/urls", this.url.addUrl);
        this.app.delete("/urls/:id", this.url.deletUrl);
    }

    private connect() {
        connect(dbConfig.url);
    }

    private listen(port: string | number) {
        this.app.listen(port, () => {
            // tslint:disable-next-line: no-console
            console.log(`Server is running on port ${port}.`);
        });
    }
}

export default new App().app;