
import mongoose, { Schema, Document } from "mongoose";

export interface Url extends Document {
  full: string;
  short: string;
}

const UrlSchema: Schema = new Schema({
  full: { type: String, required: true },
  short: { type: String, required: true, unique: true }
});

UrlSchema.path('short').validate((x: string) => x.length === 8, 'Shortened URL must be 8 characters');

const Url = mongoose.model<Url>("Url", UrlSchema);
export default Url;