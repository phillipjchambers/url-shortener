import mongoose from "mongoose";

export default (db: string) => {
  const connect = () => {
    mongoose
      .connect(db, { useNewUrlParser: true })
      .then(() => {
        // tslint:disable-next-line: no-console
        return console.log(`Successfully connected to ${db}`);
      })
      .catch(error => {
        // tslint:disable-next-line: no-console
        console.log("Error connecting to database: ", error);
        return process.exit(1);
      });
  };
  connect();

  mongoose.connection.on("disconnected", connect);
};