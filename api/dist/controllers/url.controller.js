"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUrl = exports.addUrl = exports.allUrls = void 0;
const url_1 = __importDefault(require("../models/url"));
const allUrls = (req, res) => {
    const result = url_1.default.find((err, urls) => {
        if (err) {
            res.send(err);
        }
        else {
            res.send(urls);
        }
    });
};
exports.allUrls = allUrls;
const addUrl = (req, res) => {
    const url = new url_1.default(req.body);
    url.save((err) => {
        if (err) {
            res.send(err);
        }
        else {
            res.send(url);
        }
    });
};
exports.addUrl = addUrl;
const deleteUrl = (req, res) => {
    const result = url_1.default.deleteOne({ _id: req.params.id }, (err) => {
        if (err) {
            res.send(err);
        }
        else {
            res.send("Url deleted from database");
        }
    });
};
exports.deleteUrl = deleteUrl;
//# sourceMappingURL=url.controller.js.map