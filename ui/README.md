# url-shortener-ui
Vue.js UI allowing a user to shorten URLs & view previously shortened URLS. Displays a list of shortened URLs with delete functionality.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
