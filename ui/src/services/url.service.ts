import http from "./http-common";

class UrlService {
    getAll() {
        return http.get("/urls");
    }

    create(data: Url) {
        return http.post("/urls", data);
    }

    deleteOne(id: string) {
        return http.delete(`/urls/${id}`);
    }
}

export default new UrlService();

export interface Url {
    full: string;
    short: string;
}