import { Url } from '@/services/url.service';
import { shallowMount, Wrapper } from '@vue/test-utils'
import List from '../../src/components/List.vue';

jest.mock('../../src/services/url.service', () => {
  let urls: Url[] = [
    { "full": "something.com", "short": "36966c0c" },
    { "full": "anotherthing.com", "short": "45c432cd" }
  ];

  return {
    getAll: function () {
      return new Promise(resolve => {
        resolve({
          data: urls
        })
      })
    },
    create: function (url: Url) {
      urls.push(url);
      return new Promise(resolve => {
        resolve();
      })
    },
  }
});

let wrapper: Wrapper<List>;

const validUrl = 'google.com';
const invalidUrl = 'google. com';


beforeEach(() => {
  wrapper = shallowMount(List);
});

afterEach(() => {
  wrapper.destroy();
});

describe('URL input', () => {
  test('should start empty', () => {
    expect(wrapper.find('#add-url-input').text()).toBeFalsy();
  })
});

describe('URL input', () => {
  test('should not have `valid` class applied when url is invalid', async () => {
    const textInput = wrapper.find('#add-url-input');

    await textInput.setValue(invalidUrl);

    expect((textInput.classes())).not.toContain('valid');
  })
});

describe('URL input', () => {
  test('should have `valid` class applied when url is valid', async () => {
    const textInput = wrapper.find('#add-url-input');

    await textInput.setValue(validUrl);

    expect((textInput.classes())).toContain('valid');
  })
});

describe('add URL button', () => {
  test('should be enabled when url is valid', async () => {
    const textInput = wrapper.find('#add-url-input');
    const button = wrapper.find('#add-url-button');

    await textInput.setValue(validUrl);

    expect(((button.element) as HTMLButtonElement).disabled).toBe(false);
  })
});

describe('add URL button', () => {
  test('should be disabled when url is invalid', async () => {
    const textInput = wrapper.find('#add-url-input');
    const button = wrapper.find('#add-url-button');

    await textInput.setValue(invalidUrl);

    expect(((button.element) as HTMLButtonElement).disabled).toBe(true);
  })
});

describe('URL list', () => {
  test('initially, there should be 2 urls in the list', async (done) => {
    wrapper.vm.$nextTick(() => {
      expect(wrapper.findAll('.url-list-item').length).toBe(2);
      done();
    })
  })
});

describe('URL list', () => {
  test('after adding a button click, the list should increase to 3', async (done) => {
    const textInput = wrapper.find('#add-url-input');
    const button = wrapper.find('#add-url-button');

    await textInput.setValue(validUrl);
    button.trigger('click');

    wrapper.vm.$nextTick(() => {
      expect(wrapper.findAll('.url-list-item').length).toBe(3);
      done();
    })
  })
});

describe('generated URLs', () => {
  test('each URL\'s suffix should be 8 alphanumeric characters', async () => {
    const textInput = wrapper.find('#add-url-input');
    const button = wrapper.find('#add-url-button');

    for (let index = 0; index < 100; index++) {
      await textInput.setValue(validUrl);
      button.trigger('click');
    }

    wrapper.vm.$nextTick(() => {
      const listItems = wrapper.findAll('.url-detail a');
      listItems.wrappers.forEach(wrapper => {
        const href = wrapper.attributes()['href'];
        const parts = href.split('/');
        const hash = parts[parts.length-1];
        expect(hash).toMatch(new RegExp('^[a-f0-9]{8}$'));
      });
    })
  })
});