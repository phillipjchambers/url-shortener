# url-shortener

Shortens URLs to unique 8-character strings. Runs at http://localhost:3000.

## Run using Docker

### Build
```
docker-compose build --no-cache
```

### Run
```
docker-compose up
```


